var setLayouts = debounce(function() {
	// do stuff
}, 250);

document.addEventListener('touchmove', function (e) { e.preventDefault(); }, isPassive() ? {
	capture: false,
	passive: false
} : false);

$(function(){
	FastClick.attach(document.body);
});

$(window).resize(setLayouts);

// debounce
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	}
}

// ref https://github.com/WICG/EventListenerOptions/pull/30
function isPassive() {
    var supportsPassiveOption = false;
    try {
        addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function () {
                supportsPassiveOption = true;
            }
        }));
    } catch(e) {}
    return supportsPassiveOption;
}


/*#################################
###		TOGGLE QUIZ QUESTIONS	 ###
#################################*/

const $q = $('.js-quizQuestion');
var qIndex = 0;
const $quizNav = $('.js-quizNav');
const naviItem = '<div class="quizNavigation-rect js-quizNavItem"></div>';
//add nav items to quiz navigation
for (let i=0; i<$q.length; i++) {
	$quizNav.append(naviItem);
}

const $navItems = $('.js-quizNavItem');

function showQuiz(index){
    $navItems.eq(index).addClass('isPassed');
    $q.eq(index).fadeIn(600);
}

showQuiz(qIndex);
$('.radio-label').click(function () {
    $q.eq(qIndex).fadeOut(0);
    qIndex < $q.length-1 ? qIndex++ : qIndex ;
    console.log(qIndex, $q.length);
    showQuiz(qIndex);
});





$(".js-popup").click(function (e) {


    let $target =$(e.target);
    console.log($target);
    if ($target.hasClass('popup-bg') || $target.hasClass('btnClose')) {
        $(this).fadeOut(800, function () {
            // $(this).removeClass('showPopup');
        });
    }

});

$('body').keyup(function(e){
    if(e.keyCode === 27){
        $(".js-popup").fadeOut(400, function () {
            // $(this).removeClass('showPopup');
        });

    }
});


/*#################################
###		MATCH SLIDER        	 ###
#################################*/

var imgCurrent = 0;
var $imgTot = $('#tinderslide .img').length;


$("#tinderslide").jTinder({
    // onDislike: function (item) {
    //     alert('Dislike image ' + (item.index()+1));
    // },
    // onLike: function (item) {
    //     alert('Like image ' + (item.index()+1));
    // },
    onDislike: function () {
        sliderStatus();
    },
    onLike: function () {
        sliderStatus();
    },
    animationRevertSpeed: 200,
    animationSpeed: 400,
    threshold: 1,
    likeSelector: '.like',
    dislikeSelector: '.dislike'
});

function sliderStatus (){
    if (imgCurrent < $imgTot) {
        imgCurrent++;
    };

    $('#sliderStatus').html(imgCurrent + "<span>"+ " of " +$imgTot+"</span>");
}
sliderStatus();

$('.actions .like, .actions .dislike').click(function(e){
    e.preventDefault();
    $("#tinderslide").jTinder($(this).attr('class'));
});



//ANIMATION

let w = 0;
let h =0;
let startX = 0;
let startY = 0;
let multiplier = 0;


let isClicked = false;
$imgSlides = $(".img");

$imgSlides.mouseup(function () {
    w = 0;
    h =0;
    multiplier = 0;
    isClicked = false;
    drawCircle();
});

$imgSlides.mousedown(function (e) {
    isClicked = true;
    startX = e.pageX;
    startY = e.pageY;

});

$(window).mousemove(function (e) {
    multiplier += 0.02;

    if (isClicked === true) {
        w = $imgSlides.outerWidth() + Math.abs(startX - e.pageX) * multiplier;
        // h = $imgSlides.outerWidth() + Math.abs(startY - e.pageY)*2 ;
        w = w + Math.abs(startY - e.pageY)* multiplier;
        console.log(w,h,multiplier);

        drawCircle();

    }


});

function drawCircle() {
    $('.animatedCircle').css({"width":w,"height":w});
}

/*#################################
###		GOOGLE MAPS        	     ###
#################################*/


var designWidth = 1440;

var lat = 54.689012;
var lng = 25.275627;
var zoom = 16;

function initMap() {
    if (typeof lat != 'undefined' && lat) {
        var uluru = {lat: lat, lng: lng};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: {lat: lat, lng: lng * 1.0002},
            styles: [{"elementType": "geometry.fill", "stylers": [{"color": "#f2eeeb"}]},{"featureType": "administrative", "elementType": "geometry", "stylers": [{"visibility": "off"}]},{"featureType": "administrative.locality", "elementType": "labels.text", "stylers": [{"color": "#ae252e"},{"visibility": "simplified"}]},{"featureType": "poi", "stylers": [{"visibility": "off"}]},{"featureType": "road", "stylers": [{"color": "#ffffff"},{"visibility": "simplified"}]},{"featureType": "road", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]},{"featureType": "road", "elementType": "labels.text", "stylers": [{"color": "#818181"},{"visibility": "simplified"}]},{"featureType": "transit", "stylers": [{"visibility": "off"}]},{"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}]},{"featureType": "water", "elementType": "labels.text", "stylers": [{"color": "#818181"},{"visibility": "simplified"}]}]
        });

        const markerWidth = $(this).width() / designWidth * 40;
        const markerHeight = $(this).width() / designWidth * 40;
        var mapicon = new google.maps.MarkerImage("img/googlemaps-pin.svg", null, null, null, new google.maps.Size(markerWidth,markerHeight));
        var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: mapicon
        });
    }
}

/*#################################
###		SWIPERS         	     ###
#################################*/


//Personal plan swiper

var swiper = new Swiper('.locations__slider', {
    slidesPerView: 3,
    slidesPerGroup:3,
    spaceBetween: 16,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});


