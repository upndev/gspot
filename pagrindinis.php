<?php include 'includes/header.php'; ?>
<?php include 'includes/header-block.php'; ?>



    <main class="home">

        <div class="container">
            <div class="leftBlock"></div>
            <div class="rightBlock">
                <div class="rightBlock-container">
                    <h1 class="homeHeading">
                        <span class="homeHeading--textBlock">Let's build</span>
                        pleasure roadmap for you
                    </h1>

                    <a href="#" class="btnInline">TAKE THE QUIZ
                        <span class="btnInline__arrow">
                            <svg class="btnInline__arrow--white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13" viewBox="0 0 56 13"><defs><path id="kxboa" d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"/></defs><g><g transform="translate(-1208 -516)"><use xlink:href="#kxboa"/></g></g></svg>
                        </span>
                    </a>

                    <a href="#" class="btnInline btnInline--simple">Just take me to places</a>
                    <svg class="blockBottomIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 248 100"> <defs> <linearGradient id="grd1" gradientUnits="userSpaceOnUse" x1="241.851" y1="28.098" x2="134.324" y2="-132.889"> <stop offset="0" stop-color="#c2262b" /> <stop offset="1" stop-color="#de1f26" /> </linearGradient> </defs> <g> <path d="M124.1,0.71l-2.18,0v0l-34.67,0.02l-1.06,25.56l-16.6,3.78l-5.47,-25.36l-53.9,18.19l-9.92,77.06h30.43l6.88,-56.95l13.19,-3.9l5.23,23.52l51.71,-11.37l0.14,-23.49l16.23,-0.03l16.23,0.03l0.14,23.49l51.71,11.37l5.23,-23.52l13.19,3.9l6.88,56.95h30.43l-9.92,-77.06l-53.9,-18.19l-5.47,25.36l-16.6,-3.78l-1.06,-25.56l-34.68,-0.02v0z" /> </g> </svg>

                </div>
            </div>

        </div>

    </main>
<?php include 'includes/footer-block.php'; ?>
<?php include 'includes/footer.php'; ?>