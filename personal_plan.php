<?php include 'includes/header.php'; ?>
<?php include 'includes/header-block.php'; ?>


    <main class="personalPlan">


        <div class="row">
            <h1 class="mainHeading mainHeading--sm">Your very personal<br>pleasure roadmap</h1>
        </div>
        <section class="locations">
            <div class="locations__map">
                <div id="map"></div>
            </div>


            <div class="swiper-container locations__slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="swiper-title">LOFTAS ART FACTORY</div>
                        <div class="swiper-image" style="background-image: url(img/pane/pane1.jpg)"></div>
                        <div class="swiper-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.</p>
                        </div>

                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-title">KEMPINSKI</div>
                        <div class="swiper-image" style="background-image: url(img/pane/pane2.jpg)"></div>
                        <div class="swiper-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.</p>
                        </div>

                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-title">THREE CROSSES</div>
                        <div class="swiper-image" style="background-image: url(img/pane/pane3.jpg)"></div>
                        <div class="swiper-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.</p>
                        </div>

                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-title">LOFTAS ART FACTORY</div>
                        <div class="swiper-image" style="background-image: url(img/pane/pane4.jpg)"></div>
                        <div class="swiper-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.</p>
                        </div>

                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-title">LOFTAS ART FACTORY</div>
                        <div class="swiper-image" style="background-image: url(img/pane/pane5.jpg)"></div>
                        <div class="swiper-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.</p>
                        </div>

                    </div>

                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-prev">
                    <svg class="swiper-button-icon" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" width="55" height="13" viewBox="0 0 55 13">
                        <defs>
                            <path id="yiana"
                                  d="M16.2 1033.03l5.55-5.82a.7.7 0 0 1 .98-.02c.27.24.27.7.02.96l-4.45 4.67h52c.39 0 .7.3.7.68 0 .38-.31.68-.7.68h-52l4.45 4.67c.25.26.24.71-.02.96a.7.7 0 0 1-.98-.02l-5.55-5.82a.6.6 0 0 1-.2-.47c0-.16.1-.37.2-.47z"/>
                        </defs>
                        <g>
                            <g transform="translate(-16 -1027)">
                                <use xlink:href="#yiana"/>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="swiper-button-next">
                    <svg class="swiper-button-icon" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13" viewBox="0 0 56 13">
                        <defs>
                            <path id="kxboa"
                                  d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"/>
                        </defs>
                        <g>
                            <g transform="translate(-1208 -516)">
                                <use xlink:href="#kxboa"/>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
        </section>
        <section class="attractions">
            <div class="paperPlane">
                <svg class="paperPlane__icon" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" width="345" height="237" viewBox="0 0 345 237">
                    <defs>
                        <path id="x3cpb"
                              d="M424.4 1320.66c.8-.53 1.55.57 1.05 1.23a.7.7 0 0 1-.17.75l-.56.5a427.82 427.82 0 0 0-12.32 22.39 435.69 435.69 0 0 0-33.25 87.64c-.14.52-.66.62-1.03.43-.03.5-.55.97-1.07.62-4.2-2.8-7.96-6.5-11.99-9.58a293 293 0 0 0-11.81-8.53l-3.64-2.52c.05.15.03.33-.1.53-2.35 3.6-6.24 6.65-9.47 9.46-1.7 1.46-3.43 2.88-5.2 4.25-1.55 1.2-3.13 2.72-5 3.34-.48.16-.8-.33-.69-.72 0-.03-.02-.05-.03-.08-3.7-12.95-7.03-26.09-9.27-39.39-.3-.03-.5-.3-.5-.6-11.74-5.5-22.45-14.05-31.68-23.01a.75.75 0 0 1-.14-.96.74.74 0 0 1 .38-1.08c9.92-3.8 20.24-6.75 30.33-10.05 35.3-11.54 70.56-23.16 105.91-34.5l.1-.02.15-.1M320 1389.2a.53.53 0 0 1 .46 0c7.15-5.24 14.78-9.92 22.19-14.79l77.1-50.7a4559.7 4559.7 0 0 1-24.88 8.24L342 1349.25l-26.85 8.78c-8.65 2.82-17.27 5.98-26.04 8.42 4.72 4.56 9.74 8.82 15.06 12.67 5.08 3.69 10.47 6.83 15.82 10.1m.92 1.27a860.69 860.69 0 0 0 8.5 34.79c.55-8.85.8-17.75 1.63-26.56a.83.83 0 0 1 1.11-.7c.17-.39.56-.66.97-.7.14-.04.27-.1.4-.15 1.1-1.3 2.5-2.32 3.8-3.4 1.76-1.49 3.53-2.96 5.3-4.44l9.92-8.26 36.16-30.07 18.08-15.04c2.99-2.49 5.94-5.08 8.93-7.61l-13.62 8.96-40.88 26.88c-6.81 4.48-13.62 8.97-20.44 13.44-6.58 4.32-13.06 8.87-19.86 12.86m27.5 22.8a.76.76 0 0 1 .53-.16 137.46 137.46 0 0 1-12.85-9.97c-1.15-1-2.77-2.02-3.48-3.42-.02 9.61-.98 19.34-1.75 28.93 1.15-.92 2.45-1.71 3.54-2.57a116.53 116.53 0 0 0 9.31-8.17c1.57-1.52 2.99-3.32 4.7-4.65m36.67-6.18a422.38 422.38 0 0 1 35.62-80.35c-5.2 4.47-10.7 8.68-15.94 13.04l-35.8 29.77-17.74 14.73-9.23 7.66c-2.63 2.19-5.35 5.2-8.51 6.56h-.01c-.1.1-.22.16-.34.22 1.7.53 3.14 2.43 4.44 3.55 1.7 1.47 3.44 2.9 5.2 4.28 4.02 3.14 8.23 5.99 12.42 8.88 4.02 2.78 7.97 5.65 11.83 8.63 3.58 2.76 7.59 5.49 10.69 8.8l.01-.1c2.16-8.63 4.62-17.2 7.36-25.67"/>
                        <path id="x3cpa" d="M81 1429.56h244.56V1557H81z"/>
                        <path id="x3cpd"
                              d="M324.99 1429.76c.13-.35.67-.2.55.15a84.1 84.1 0 0 1-26.65 37.9c-2.82 2.27-5.96 4.55-9.45 5.68a28.5 28.5 0 0 1-2.16 5c-2.36 4.39-5.2 8.57-8.07 12.63-5.67 8.03-12.11 15.56-19.56 21.99-11.2 9.66-25.18 16.97-39.93 19a43.25 43.25 0 0 1-1.93 4.1c-8.83 16.36-28 25.32-45.79 18.48-8.26-3.18-15.44-9.41-18.85-17.7a25.56 25.56 0 0 1-1.12-3.39c-1.2.43-2.38.8-3.5 1.13a54 54 0 0 1-31.4-.28c-18.82-5.92-33.36-22.98-36.12-42.59-.1-.73 1-1.06 1.13-.31 3.19 18.12 15.48 34.17 32.87 40.61a53.04 53.04 0 0 0 26.11 2.72c3.6-.54 7.17-1.45 10.59-2.74-.63-3.3-.53-6.76.74-9.89 1.25-3.07 4.38-5.67 7.5-3.18 2.9 2.3 2.9 6.32.74 9.11-1.78 2.3-4.52 3.82-7.36 4.94.76 3.07 2.17 6 3.96 8.57 4.38 6.3 11.13 10.7 18.47 12.7 15.42 4.24 31.12-3.32 39.47-16.6a45.78 45.78 0 0 0 2.94-5.49c-4.03.45-8.1.51-12.2.1-6.67-.67-15.2-3.8-14.92-11.9.1-2.74 1.35-5.56 3.92-6.79 3.05-1.45 6.25-.17 8.97 1.57a7.81 7.81 0 0 1 1.72-7c2.85-3.34 7.67-3.06 11.1-.78 7.58 5.02 6.5 15.2 3.5 23.25l.8-.12c16.16-2.64 30.69-11.28 42.32-22.62a128.1 128.1 0 0 0 15.26-18.13c2.26-3.22 4.42-6.5 6.44-9.87 1.13-1.9 2.3-3.98 3.07-6.17-.75.17-1.5.3-2.28.35a9.1 9.1 0 0 1-8.49-4.05c-1.48-2.24-2.15-5.3-.77-7.74 3.42-6.04 11.76-1.36 13.18 3.8.52 1.86.46 3.77.08 5.64 3.32-1.3 6.55-3.51 8.8-5.3a84.74 84.74 0 0 0 26.32-36.78m-172.22 99.76c.04.7.13 1.4.25 2.1l.13-.06c3.82-1.59 10.08-5.32 6.95-10.28-2.05-3.24-5.17-1.38-6.34 1.47a15.3 15.3 0 0 0-1 6.77m54.67 1.76c3.77.26 7.57.14 11.32-.33 2.47-6 3.67-12.94.8-18.4-1.94-3.66-6.91-6.97-11.06-4.84-3.28 1.68-4.8 6.62-2 9.37.53.52-.19 1.5-.8 1.04-3.42-2.6-9.58-6.34-12.5-1.03-1.87 3.4-.75 7.7 2.05 10.18 3.28 2.91 7.97 3.71 12.2 4m70.34-62.9c2.34 4.88 6.57 5.27 10.84 3.92.43-1.72.57-3.49.21-5.25-.66-3.24-3.84-6.34-7.26-6.32-4.2.03-5.34 4.4-3.79 7.64"/>
                        <clipPath id="x3cpc">
                            <use xlink:href="#x3cpa"/>
                        </clipPath>
                    </defs>
                    <g>
                        <g transform="translate(-81 -1320)">
                            <g>
                                <g>
                                    <use fill="#ae252e" xlink:href="#x3cpb"/>
                                </g>
                            </g>
                            <g>
                                <g/>
                                <g clip-path="url(#x3cpc)">
                                    <use fill="#ae252e" xlink:href="#x3cpd"/>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>

            <h2 class="secondHeading ml">You may start planning<br>your trip to Vilnius</h2>
            <a href="#" class="btnInline btnInline--simple ml mb">Check flight tickets
                <span class="btnInline__arrow">
                                <svg class="btnInline__arrow--red" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13"
                                     viewBox="0 0 56 13"><defs><path id="kxboa"
                                                                     d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"/></defs><g><g
                                                transform="translate(-1208 -516)"><use
                                                    xlink:href="#kxboa"/></g></g></svg>
                            </span>
            </a>

            <div class="container">
                <div class="left">
                    <h3 class="sectionHeading sectionHeading--vLine">
                        <span class="sectionHeading--fancy">Do it in the dark</span>
                        Best nightlife spots to do it till the<br>morning.
                    </h3>
                </div>
                <div class="right">
                    <div class="wrapper">
                        <div class="card">
                            <h4 class="cardTitle">LOFTAS ART FACTORY</h4>
                            <div class="cardImage" style="background-image: url(images/personal-1.jpg)"></div>
                            <div class="cardText">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                            </div>
                            <div class="cardCTA">
                                <a href="#" class="btnInline btnInline--simple btnInline--red">More info
                                    <span class="btnInline__arrow btnInline__arrow--red">
                                             <svg class="btnInline__icon" xmlns="http://www.w3.org/2000/svg"
                                                  xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13"
                                                  viewBox="0 0 56 13"><defs><path id="kxboa"
                                                                                  d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"></path></defs><g><g
                                                             transform="translate(-1208 -516)"><use
                                                                 xlink:href="#kxboa"></use></g></g></svg>
                                        </span>
                                </a>

                                <a href="#">
                                    <svg class="icon--small" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20"
                                         width="22" height="20">
                                        <path
                                                d="M19.98,10.98c1.21,-1.19 1.88,-2.78 1.88,-4.47c0,-1.69 -0.67,-3.28 -1.88,-4.47c-2.46,-2.43 -6.44,-2.46 -8.95,-0.1c-1.2,-1.13 -2.76,-1.75 -4.42,-1.75c-1.71,0 -3.32,0.66 -4.52,1.85c-2.5,2.47 -2.49,6.48 0,8.94l7.9,7.77l1.06,-0.93l-7.9,-7.89c-1.91,-1.89 -1.91,-4.96 0,-6.85c0.92,-0.91 2.15,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42l0.96,0.95l0.95,-0.95c0.93,-0.91 2.16,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42c0.93,0.92 1.44,2.13 1.44,3.42c0,1.29 -0.51,2.51 -1.44,3.42l-8.93,8.82l1.06,1.05z"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="wrapper">
                        <div class="card">
                            <h4 class="cardTitle">OPIUM CLUB</h4>
                            <div class="cardImage" style="background-image: url(images/personal-2.jpg)"></div>
                            <div class="cardText">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                            </div>
                            <div class="cardCTA">
                                <a href="#" class="btnInline btnInline--simple btnInline--red">More info
                                    <span class="btnInline__arrow btnInline__arrow--red">
                                             <svg class="btnInline__icon" xmlns="http://www.w3.org/2000/svg"
                                                  xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13"
                                                  viewBox="0 0 56 13"><defs><path id="kxboa"
                                                                                  d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"></path></defs><g><g
                                                             transform="translate(-1208 -516)"><use
                                                                 xlink:href="#kxboa"></use></g></g></svg>
                                        </span>
                                </a>

                                <a href="#">
                                    <svg class="icon--small" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20"
                                         width="22" height="20">
                                        <path
                                                d="M19.98,10.98c1.21,-1.19 1.88,-2.78 1.88,-4.47c0,-1.69 -0.67,-3.28 -1.88,-4.47c-2.46,-2.43 -6.44,-2.46 -8.95,-0.1c-1.2,-1.13 -2.76,-1.75 -4.42,-1.75c-1.71,0 -3.32,0.66 -4.52,1.85c-2.5,2.47 -2.49,6.48 0,8.94l7.9,7.77l1.06,-0.93l-7.9,-7.89c-1.91,-1.89 -1.91,-4.96 0,-6.85c0.92,-0.91 2.15,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42l0.96,0.95l0.95,-0.95c0.93,-0.91 2.16,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42c0.93,0.92 1.44,2.13 1.44,3.42c0,1.29 -0.51,2.51 -1.44,3.42l-8.93,8.82l1.06,1.05z"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="left">
                    <h3 class="sectionHeading sectionHeading--vLine">
                        <span class="sectionHeading--fancy">Use your tongue</span>
                        Extraordinary tastes to leave you<br>
                        wanting for more.
                    </h3>
                </div>
                <div class="right">
                    <div class="wrapper">
                        <div class="card">
                            <h4 class="cardTitle">LOFTAS ART FACTORY</h4>
                            <div class="cardImage" style="background-image: url(images/personal-1.jpg)"></div>
                            <div class="cardText">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                            </div>
                            <div class="cardCTA">
                                <a href="#" class="btnInline btnInline--simple btnInline--red">More info
                                    <span class="btnInline__arrow btnInline__arrow--red">
                                             <svg class="btnInline__icon" xmlns="http://www.w3.org/2000/svg"
                                                  xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13"
                                                  viewBox="0 0 56 13"><defs><path id="kxboa"
                                                                                  d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"></path></defs><g><g
                                                             transform="translate(-1208 -516)"><use
                                                                 xlink:href="#kxboa"></use></g></g></svg>
                                        </span>
                                </a>

                                <a href="#">
                                    <svg class="icon--small" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20"
                                         width="22" height="20">
                                        <path
                                                d="M19.98,10.98c1.21,-1.19 1.88,-2.78 1.88,-4.47c0,-1.69 -0.67,-3.28 -1.88,-4.47c-2.46,-2.43 -6.44,-2.46 -8.95,-0.1c-1.2,-1.13 -2.76,-1.75 -4.42,-1.75c-1.71,0 -3.32,0.66 -4.52,1.85c-2.5,2.47 -2.49,6.48 0,8.94l7.9,7.77l1.06,-0.93l-7.9,-7.89c-1.91,-1.89 -1.91,-4.96 0,-6.85c0.92,-0.91 2.15,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42l0.96,0.95l0.95,-0.95c0.93,-0.91 2.16,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42c0.93,0.92 1.44,2.13 1.44,3.42c0,1.29 -0.51,2.51 -1.44,3.42l-8.93,8.82l1.06,1.05z"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="wrapper">
                        <div class="card">
                            <h4 class="cardTitle">OPIUM CLUB</h4>
                            <div class="cardImage" style="background-image: url(images/personal-2.jpg)"></div>
                            <div class="cardText">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                            </div>
                            <div class="cardCTA">
                                <a href="#" class="btnInline btnInline--simple btnInline--red">More info
                                    <span class="btnInline__arrow btnInline__arrow--red">
                                             <svg class="btnInline__icon" xmlns="http://www.w3.org/2000/svg"
                                                  xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13"
                                                  viewBox="0 0 56 13"><defs><path id="kxboa"
                                                                                  d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"></path></defs><g><g
                                                             transform="translate(-1208 -516)"><use
                                                                 xlink:href="#kxboa"></use></g></g></svg>
                                        </span>
                                </a>

                                <a href="#">
                                    <svg class="icon--small" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20"
                                         width="22" height="20">
                                        <path
                                                d="M19.98,10.98c1.21,-1.19 1.88,-2.78 1.88,-4.47c0,-1.69 -0.67,-3.28 -1.88,-4.47c-2.46,-2.43 -6.44,-2.46 -8.95,-0.1c-1.2,-1.13 -2.76,-1.75 -4.42,-1.75c-1.71,0 -3.32,0.66 -4.52,1.85c-2.5,2.47 -2.49,6.48 0,8.94l7.9,7.77l1.06,-0.93l-7.9,-7.89c-1.91,-1.89 -1.91,-4.96 0,-6.85c0.92,-0.91 2.15,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42l0.96,0.95l0.95,-0.95c0.93,-0.91 2.16,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42c0.93,0.92 1.44,2.13 1.44,3.42c0,1.29 -0.51,2.51 -1.44,3.42l-8.93,8.82l1.06,1.05z"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="wrapper">
                        <div class="card">
                            <h4 class="cardTitle">OPIUM CLUB</h4>
                            <div class="cardImage" style="background-image: url(images/personal-2.jpg)"></div>
                            <div class="cardText">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                            </div>
                            <div class="cardCTA">
                                <a href="#" class="btnInline btnInline--simple btnInline--red">More info
                                    <span class="btnInline__arrow btnInline__arrow--red">
                                             <svg class="btnInline__icon" xmlns="http://www.w3.org/2000/svg"
                                                  xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13"
                                                  viewBox="0 0 56 13"><defs><path id="kxboa"
                                                                                  d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"></path></defs><g><g
                                                             transform="translate(-1208 -516)"><use
                                                                 xlink:href="#kxboa"></use></g></g></svg>
                                        </span>
                                </a>

                                <a href="#">
                                    <svg class="icon--small" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20"
                                         width="22" height="20">
                                        <path
                                                d="M19.98,10.98c1.21,-1.19 1.88,-2.78 1.88,-4.47c0,-1.69 -0.67,-3.28 -1.88,-4.47c-2.46,-2.43 -6.44,-2.46 -8.95,-0.1c-1.2,-1.13 -2.76,-1.75 -4.42,-1.75c-1.71,0 -3.32,0.66 -4.52,1.85c-2.5,2.47 -2.49,6.48 0,8.94l7.9,7.77l1.06,-0.93l-7.9,-7.89c-1.91,-1.89 -1.91,-4.96 0,-6.85c0.92,-0.91 2.15,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42l0.96,0.95l0.95,-0.95c0.93,-0.91 2.16,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42c0.93,0.92 1.44,2.13 1.44,3.42c0,1.29 -0.51,2.51 -1.44,3.42l-8.93,8.82l1.06,1.05z"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="left">
                    <h3 class="sectionHeading sectionHeading--vLine">
                        <span class="sectionHeading--fancy">Go wild</span>
                        Green spots where you can always<br>
                        get behind the bush.
                    </h3>
                </div>
                <div class="right">
                    <div class="wrapper">
                        <div class="card">
                            <h4 class="cardTitle">LOFTAS ART FACTORY</h4>
                            <div class="cardImage" style="background-image: url(images/personal-1.jpg)"></div>
                            <div class="cardText">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                            </div>
                            <div class="cardCTA">
                                <a href="#" class="btnInline btnInline--simple btnInline--red">More info
                                    <span class="btnInline__arrow btnInline__arrow--red">
                                             <svg class="btnInline__icon" xmlns="http://www.w3.org/2000/svg"
                                                  xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13"
                                                  viewBox="0 0 56 13"><defs><path id="kxboa"
                                                                                  d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"></path></defs><g><g
                                                             transform="translate(-1208 -516)"><use
                                                                 xlink:href="#kxboa"></use></g></g></svg>
                                        </span>
                                </a>

                                <a href="#">
                                    <svg class="icon--small" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20"
                                         width="22" height="20">
                                        <path
                                                d="M19.98,10.98c1.21,-1.19 1.88,-2.78 1.88,-4.47c0,-1.69 -0.67,-3.28 -1.88,-4.47c-2.46,-2.43 -6.44,-2.46 -8.95,-0.1c-1.2,-1.13 -2.76,-1.75 -4.42,-1.75c-1.71,0 -3.32,0.66 -4.52,1.85c-2.5,2.47 -2.49,6.48 0,8.94l7.9,7.77l1.06,-0.93l-7.9,-7.89c-1.91,-1.89 -1.91,-4.96 0,-6.85c0.92,-0.91 2.15,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42l0.96,0.95l0.95,-0.95c0.93,-0.91 2.16,-1.42 3.46,-1.42c1.31,0 2.54,0.5 3.47,1.42c0.93,0.92 1.44,2.13 1.44,3.42c0,1.29 -0.51,2.51 -1.44,3.42l-8.93,8.82l1.06,1.05z"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


        </section>


    </main>
<?php include 'includes/footer-block.php'; ?>
<?php include 'includes/footer.php'; ?>