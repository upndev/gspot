<footer class="footer">
    <div class="footerText footerText--left">
        <p>VŠĮ "GO VILNIUS"   Gynėjų g. 14, LT-01109 Vilnius, Lithuania <a href="mailto:go@vilnius.lt">go@vilnius.lt</a></p>
    </div>
<!--    <div class="footerLogo">-->
<!--        <svg id="footerIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 248 100" > <defs> <linearGradient id="grd1" gradientUnits="userSpaceOnUse" x1="241.851" y1="28.098" x2="134.324" y2="-132.889"> <stop offset="0" stop-color="#c2262b" /> <stop offset="1" stop-color="#de1f26" /> </linearGradient> </defs> <g> <path d="M124.1,0.71l-2.18,0v0l-34.67,0.02l-1.06,25.56l-16.6,3.78l-5.47,-25.36l-53.9,18.19l-9.92,77.06h30.43l6.88,-56.95l13.19,-3.9l5.23,23.52l51.71,-11.37l0.14,-23.49l16.23,-0.03l16.23,0.03l0.14,23.49l51.71,11.37l5.23,-23.52l13.19,3.9l6.88,56.95h30.43l-9.92,-77.06l-53.9,-18.19l-5.47,25.36l-16.6,-3.78l-1.06,-25.56l-34.68,-0.02v0z" /> </g> </svg>-->
<!--    </div>-->
    <div class="footerText footerText--right">
        <p>Copyright (C) 2018 Go Vilnius</p>
    </div>
</footer>