<div class="popup-bg js-popup">
    <div class="popup-container">
        <div class="btnClose">
            <svg class="closeIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17" width="17" height="17"><path d="M16.82,1.48l-1.29,-1.29l-7.02,7.02l-7.02,-7.02l-1.29,1.29l7.02,7.02l-7.02,7.02l1.29,1.29l7.02,-7.02l7.02,7.02l1.29,-1.29l-7.02,-7.02z"/></svg>
        </div>
        <h3 class="popup-heading">Don't forget to email roadmap to yourself!</h3>
        <div class="popup-textBlock">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
        </div>
        <form action="" class="popup-form">
            <div class="formControl">
                <input class="popup-input popup-input--email" type="email" placeholder="Email">
                <input class="popup-input popup-input--submit" type="submit" value="Send">
            </div>

            <div class="popup-checkbox">
                <input type="checkbox" class="checkbox-input" value="selfempl" id="selfEmployer" name="selfEmployer">
                <label for="selfEmployer" class="checkbox-label">
                    <span class="checkbox-button">
                        <svg class="checkbox-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 16"><path d="M5.43,10.9l-3.34,-3.4l-2.08,2.14l5.43,5.36l11.24,-12.86l-2.1,-2.14z"/></svg>
                    </span>
                    I would like to receive more news about Vilnius
                </label>
            </div>
        </form>

        <h3 class="popup-heading u-mb">Or share with your friends</h3>
        <div class="popup-socialBlock">
            <a href="#" class="socialIcon-link">
            <span class="socialIcon-roundCircle">
                <svg class="socialIcon-icon socialIcon-icon--fb" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M304 96h80v-96h-80c-61.757 0-112 50.243-112 112v48h-64v96h64v256h96v-256h80l16-96h-96v-48c0-8.673 7.327-16 16-16z"></path></svg>
            </span>

            </a>
            <a href="#" class="socialIcon-link">
            <span class="socialIcon-roundCircle">
                <svg class="socialIcon-icon socialIcon-icon--tw" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 12"><path d="M14.75,1.43c-0.54,0.24 -1.13,0.4 -1.74,0.48c0.62,-0.37 1.1,-0.97 1.33,-1.67c-0.59,0.35 -1.23,0.6 -1.92,0.73c-0.55,-0.59 -1.34,-0.96 -2.21,-0.96c-1.67,0 -3.03,1.35 -3.03,3.02c0,0.24 0.03,0.47 0.08,0.69c-2.51,-0.13 -4.74,-1.33 -6.23,-3.16c-0.26,0.45 -0.41,0.97 -0.41,1.52c0,1.05 0.53,1.98 1.35,2.52c-0.5,-0.02 -0.96,-0.15 -1.37,-0.38v0.04c0,1.47 1.04,2.69 2.43,2.97c-0.25,0.07 -0.52,0.11 -0.8,0.11c-0.2,0 -0.38,-0.02 -0.57,-0.06c0.39,1.2 1.5,2.08 2.83,2.1c-1.04,0.81 -2.34,1.29 -3.76,1.29c-0.24,0 -0.48,-0.01 -0.72,-0.04c1.34,0.86 2.93,1.36 4.64,1.36c5.57,0 8.61,-4.61 8.61,-8.61l-0.01,-0.39c0.59,-0.42 1.11,-0.96 1.51,-1.56z"/></svg>
            </span>

            </a>
        </div>
<!--        <svg id="popup-footerIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 248 100"> <defs> <linearGradient id="grd1" gradientUnits="userSpaceOnUse" x1="241.851" y1="28.098" x2="134.324" y2="-132.889"> <stop offset="0" stop-color="#c2262b" /> <stop offset="1" stop-color="#de1f26" /> </linearGradient> </defs> <g> <path d="M124.1,0.71l-2.18,0v0l-34.67,0.02l-1.06,25.56l-16.6,3.78l-5.47,-25.36l-53.9,18.19l-9.92,77.06h30.43l6.88,-56.95l13.19,-3.9l5.23,23.52l51.71,-11.37l0.14,-23.49l16.23,-0.03l16.23,0.03l0.14,23.49l51.71,11.37l5.23,-23.52l13.19,3.9l6.88,56.95h30.43l-9.92,-77.06l-53.9,-18.19l-5.47,25.36l-16.6,-3.78l-1.06,-25.56l-34.68,-0.02v0z" /> </g> </svg>-->

        
    </div>
</div>