<?php include 'includes/header.php'; ?>
<?php include 'includes/header-block.php'; ?>



    <main class="placeInformation">

        <div class="sliderContainer">
            <img class="sliderContainer__img" src="images/12-layers.jpg" alt="">
        </div>

        <section class="about">
            <div class="row">
                <h3 class="paragraphTitle">ABOUT</h3>
            </div>
            <div class="row">
                <div class="wrapper">
                    <div class="textBlock">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan.
                        </p>
                    </div>
                </div>
                <div class="wrapper">
                    <h4 class="sectionHeading">
                        <span class="sectionHeading--fancy">Get directions</span>
                        Find the fastest and the most pleasure way to your destination!
                    </h4>
                    <a href="#" class="btnInline btnInline--simple btnInline--red">Directions
                        <span class="btnInline__arrow btnInline__arrow--red">
                            <svg class="btnInline__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="56" height="13" viewBox="0 0 56 13"><defs><path id="kxboa" d="M1263.8 522.03l-5.6-5.82a.71.71 0 0 0-.98-.02c-.27.24-.28.7-.03.96l4.5 4.67h-52.47a.7.7 0 0 0-.7.68c0 .38.31.68.7.68h52.47l-4.5 4.67a.7.7 0 0 0 .03.96c.27.27.73.24.99-.02l5.6-5.82a.6.6 0 0 0 .2-.47.8.8 0 0 0-.2-.47z"/></defs><g><g transform="translate(-1208 -516)"><use xlink:href="#kxboa"/></g></g></svg>
                        </span>
                    </a>

                </div>
            </div>
        </section>
        <section class="map">
            <div id="map"></div>
            <ul class="markerData">
                <li class="markerData__item markerData__item--street">Street 12-123, 12345 Vilnius,<br>Lithuania</li>
                <li class="markerData__item markerData__item--time">I-V  10:00-18:00   VI 10:00-15:000</li>
                <li class="markerData__item markerData__item--phone"><a href="tel:+370 612 34567">+370 612 34567</a></li>
                <li class="markerData__item markerData__item--url"><a href="http://www.place.lt">http://www.place.lt</a></li>
            </ul>



        </section>



    </main>
<?php include 'includes/footer-block.php'; ?>
<?php include 'includes/footer.php'; ?>