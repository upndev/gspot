<?php include 'includes/header.php'; ?>
<?php include 'includes/header-block.php'; ?>


<main class="main">
    <div class="quizContainer">
        <h1 class="mainHeading">Tell me what<br>you like:</h1>
        <form action="#">
            <section class="quizContent js-quizQuestion">
                <h2 class="secondHeading">There's no pleasure without a little cardio</h2>
                <div class="textDescription">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                </div>
                <div class="quizCta quiz-1">
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input radio-input--1" value="true" id="radio-1-true" name="radio-1">
                        <label for="radio-1-true" class="radio-label">True</label>
                    </div>
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input radio-input--1" value="false" id="radio-1-false" name="radio-1">
                        <label for="radio-1-false" class="radio-label isFalse">False</label>
                    </div>
                </div>
            </section>

            <section class="quizContent js-quizQuestion">
                <h2 class="secondHeading">I like doing it in the dark</h2>
                <div class="textDescription">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                </div>
                <div class="quizCta quiz-2">
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input" value="1" id="radio-2-1" name="radio-2">
                        <label for="radio-2-1" class="radio-label">
                            <span class="radio-button"></span>
                        </label>
                    </div>
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input" value="2" id="radio-2-2" name="radio-2">
                        <label for="radio-2-2" class="radio-label">
                            <span class="radio-button"></span>
                        </label>
                    </div>
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input" value="3" id="radio-2-3" name="radio-2">
                        <label for="radio-2-3" class="radio-label">
                            <span class="radio-button"></span>
                        </label>
                    </div>
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input" value="4" id="radio-2-4" name="radio-2">
                        <label for="radio-2-4" class="radio-label">
                            <span class="radio-button"></span>
                        </label>
                    </div>
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input" value="5" id="radio-2-5" name="radio-2">
                        <label for="radio-2-5" class="radio-label">
                            <span class="radio-button"></span>
                        </label>
                    </div>
                    <span class="quizCta-radioLabel quizCta-radioLabel--left">Disagree</span>
                    <span class="quizCta-radioLabel quizCta-radioLabel--right">Agree</span>
                </div>
            </section>
            <section class="quizContent js-quizQuestion">
                <h2 class="secondHeading">Question 3</h2>
                <div class="textDescription">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
                </div>
                <div class="quizCta quiz-1">
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input radio-input--1" value="true" id="radio-3-true" name="radio-3">
                        <label for="radio-3-true" class="radio-label">True</label>
                    </div>
                    <div class="quizCta-radio">
                        <input type="radio" class="radio-input radio-input--1" value="false" id="radio-3-false" name="radio-3">
                        <label for="radio-3-false" class="radio-label isFalse">False</label>
                    </div>
                </div>
            </section>
        </form>


        <div class="quizNavigation js-quizNav"></div>
    </div>


</main>







<?php include 'includes/footer-block.php'; ?>

<?php include 'includes/footer.php'; ?>