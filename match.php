<div id="page">


<?php include 'includes/header.php'; ?>
<?php include 'includes/header-block.php'; ?>


<main class="main match">
    <div class="matchContainer">
        <h1 class="mainHeading--small">LET'S GET YOU MATCHED</h1>
        <p class="textDescription">Drag it to right or left</p>

        <div class="slidesWraper">
            <div id="tinderslide">
                <ul>
                    <li>
                        <div class="img" style="background-image: url(img/pane/pane1.jpg)"></div>
                        <div class="slideTitle">Vilnius last</div>
                        <div class="like"></div>
                        <div class="dislike"></div>
                    </li>
                    <li>
                        <div class="img" style="background-image: url(img/pane/pane2.jpg)"></div>
                        <div class="slideTitle">Vilnius</div>
                        <div class="like"></div>
                        <div class="dislike"></div>
                    </li>
                    <li>
                        <div class="img" style="background-image: url(img/pane/pane3.jpg)"></div>
                        <div class="slideTitle">Vilnius 1</div>
                        <div class="like"></div>
                        <div class="dislike"></div>
                    </li>
                    <li class="pane2">
                        <div class="img" style="background-image: url(img/pane/pane4.jpg)"></div>
                        <div class="slideTitle">Vilnius 2</div>
                        <div class="like"></div>
                        <div class="dislike"></div>
                    </li>
                    <li class="pane1">
                        <div class="img" style="background-image: url(img/pane/pane5.jpg)"></div>
                        <div class="slideTitle">Vilnius pirma</div>
                        <div class="like"></div>
                        <div class="dislike"></div>
                    </li>
                </ul>
            </div>
            <div class="actions">
                <div class="actionsBtn actionsBtn--left">
                    <div class="actionsText actionsText--style">Leave it</div>
                    <span class="iconCircle">
                            <svg class="dislikeIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17"><path d="M16.82,1.48l-1.29,-1.29l-7.02,7.02l-7.02,-7.02l-1.29,1.29l7.02,7.02l-7.02,7.02l1.29,1.29l7.02,-7.02l7.02,7.02l1.29,-1.29l-7.02,-7.02z"/></svg>
                    </span>
                    <a class="dislike" href="#"></a>
                </div>
                <div class="actionsBtn actionsBtn--right">
                    <div class="actionsText">Like it</div>
                    <span class="iconCircle">
                            <svg class="likeIcon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 34 29"><defs><path id="sz5wa" d="M1110.17 512.97c-1.07-1.93-7.8-8.38-15.6.96-8.19-9.34-14.53-2.89-15.6-.96-1.95 3.56-.78 8.95 1.95 11.55l13.65 13.48 13.66-13.48c2.72-2.6 3.9-7.99 1.94-11.55z"/></defs><g><g transform="translate(-1078 -509)"><use xlink:href="#sz5wa"/></g></g></svg>
                    </span>
                    <a class="like" href="#"></a>
                </div>
            </div>
            <div class="animatedCircle"></div>

        </div>
        <div id="sliderStatus"></div>
    </div>





</main>


<?php include 'includes/footer-block.php'; ?>
<?php include 'includes/footer.php'; ?>

</div>
